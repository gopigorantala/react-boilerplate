1. npm init => follow steps to create a node project
2. npm install --save gulp@3.9.0 gulp-connect@2.2.0 gulp-open@1.0.0
3. npm install -g gulp

#Gulp
Now that you have all these.. 
create 2 folders
1. src  ==> all source related codes
2. dist ==>  using build process we write our files to dist folder, this gets pushed to application or web
Now create a file called
 - gulpfile.js 
 Add all the tasks here..
 
 #Browserify
 
 npm install browserify@11.0.1 reactify@1.1.1 vinyl-source-stream@1.1.0
 
 - after installing add them in gulpfile.js
 
 //START gulpfile.js ...........................................
 var browserify = require('browserify'); // Bundle JS
 var reactify = require('reactify'); // Transforms React JSX to JS
 var source = require('vinyl-source-stream'); // Use conventional text streams with Gulp

 
 //JS task  -- added after adding Browserify
 gulp.task('js', function () {
     browserify(config.paths.mainJs)
         .transform(reactify)
         .bundle()
         .on('error', console.error.bind(console))
         .pipe(source('bundle.js'))
         .pipe(gulp.dest(config.paths.dist + '/scripts'))
         .pipe(connect.reload());
 });

//watch files  -- below addded JS entry
gulp.task('watch', function () {
    gulp.watch(config.paths.html, ['html']);
    gulp.watch(config.paths.js, ['js']);
});

//default task  -- below added js entry
gulp.task('default', ['html', 'js', 'open', 'watch']);
//END......................................................

Now add the bundle.js in HTML (reference it)

#BootStrap and jQuery

npm install --save bootstrap@3.3.5 jquery@2.1.4 gulp-concat@2.6.0

add jquery as global require function

#Eslint
npm i --save gulp-eslint@0.15.0

add the task and in default gulp.. then add the lint in the "watch"

Now create a JSON file and add the rules eslint.config.json

#REACT:
npm install --save react@0.13.3 react-router@0.13.3 flux@2.0.3